<?php

/**
 * @file
 * Contains \Drupal\block_current_date\Plugin\Block\DonBlock.
 */

namespace Drupal\last_updated_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
/**
 * Provides a last updated content.
 *
 * @Block(
 *   id = "last_updated_content",
 *   admin_label = @Translation("last updated content"),
 *   category = @Translation("last updated content node")
 * )
 */
class LastUpdatedContent extends BlockBase implements BlockPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    $default_config = \Drupal::config('last_updated_content.settings');
    return [
      'format' => $default_config->get('format'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $config = $this->getConfiguration();
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $query = $storage->getQuery();
    $query->condition('status', 1)
      ->sort('changed', 'DESC')
      ->range(0, 1);
    $result = $query->execute();
    foreach ($result as $nid) {
      $node = Node::load($nid);
      $updated = date($config['format'], $node->changed->value);
    }

    return [
      '#theme' => 'last_updated_content',
      '#lastDate' => $updated,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['format'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#title' => $this->t('Format'),
      '#description' => $this->t('Format date: Example l, F j, Y - H:i'),
      '#default_value' => isset($config['format']) ? $config['format'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['format'] = $values['format'];
  }


}

